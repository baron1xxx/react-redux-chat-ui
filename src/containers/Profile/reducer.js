import {SET_USER} from './actionTypes';

// eslint-disable-next-line
export default (state = {}, {type, data}) => {

    switch (type) {
        case SET_USER: {
            return {
                ...state,
                ...data
            }
        }
        default: {
            return state
        }
    }
};
