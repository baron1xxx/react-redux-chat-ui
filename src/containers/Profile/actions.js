import * as userService from '../../services/userService'
import {SET_USER} from './actionTypes'

export const setUser = user => ({
    type: SET_USER,
    data: user
});

export const loadUser = () => dispatch => {
    const user = userService.getUser();
    dispatch(setUser(user));
};
