import * as messageService from '../../services/messageServise'
import {
    SET_MESSAGES,
    ADD_MESSAGE,
    DELETE_MESSAGE
} from './actionTypes'

export const setMessagesAction = messages => ({
    type: SET_MESSAGES,
    data: messages
});

const addMessageAction = message => ({
    type: ADD_MESSAGE,
    data: message
});

const deleteMessageAction = messageId => ({
    type: DELETE_MESSAGE,
    data: messageId
});

export const loadMessages = () => async dispatch => {
    const messages = await messageService.getMessages();
    dispatch(setMessagesAction(messages));
};

export const addMessage = (message, authUser) => dispatch => {
    const newMessages =  messageService.addMessage(message, authUser);
    dispatch(addMessageAction(newMessages));
};

export const editMessage = (text, messageId) => (dispatch, getRootState) => {
    const {chat: { messages}} = getRootState();
    const newMessages = messages.map(message => message.id === messageId ? {...message, text} : message);
    dispatch(setMessagesAction(newMessages));
};

export const deleteMessage = messageId => dispatch => {
    dispatch(deleteMessageAction(messageId));
};

export const reactMessage = messageId => (dispatch, getRootState) => {
    const {chat: { messages}} = getRootState();
    const newMessages = messages.map(message => message.id === messageId ? {...message, isLiked: !message.isLiked} : message);
    dispatch(setMessagesAction(newMessages));
};
