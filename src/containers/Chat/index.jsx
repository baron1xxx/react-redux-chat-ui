import React, {useEffect, useState, useRef} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {loadUser} from '../Profile/actions'
import {loadMessages, addMessage, editMessage, deleteMessage, reactMessage} from '../Chat/actions'

import ChatFooter from '../../components/ChatFooter';
import ChatHeader from '../../components/ChatHeader';
import MessagesList from '../../components/MessagesList';
import Spinner from '../../components/Spinner';
import EditMessage from '../../components/EditMessage';
import DeleteMessage from "../../components/DeleteMessage";

import styles from './styles.module.css';

const Chat = (props) => {
    const {
        messages,
        countMessages,
        countUsers,
        dateLastMessage,
        user: authUser,
        isLoaded,
        loadUser,
        loadMessages,
        addMessage,
        editMessage,
        deleteMessage,
        reactMessage
    } = props;

    const inputRef = useRef(null);

    const [editedMessage, setEditedMessage] = useState(undefined);
    const [deletedMessage, setDeleteDMessage] = useState(undefined);

    useEffect(() => {
        loadUser();
        loadMessages();
    }, [loadMessages, loadUser]);

    useEffect(() => {
        inputRef.current.focus();
    }, [messages]);

    const setEditStatusMessage = message => {
        setEditedMessage(message);
    };

    const setDeleteStatusMessage = messageId => {
        setDeleteDMessage(messageId);
    };

    return (
        <div className={styles.chatContainer}>
            <ChatHeader
                authUser={props.user}
                countMessages={countMessages}
                countUsers={countUsers}
                dateLustMessage={dateLastMessage}
            />
            {isLoaded
                ? <MessagesList
                    messages={messages}
                    authUser={authUser}
                    reactMessage={reactMessage}
                    setEditStatusMessage={setEditStatusMessage}
                    setDeleteStatusMessage={setDeleteStatusMessage}
                />
                : <Spinner/>}
            <ChatFooter
                addMessage={addMessage}
                authUser={authUser}
                inputRef={inputRef}/>

            {editedMessage &&
            <EditMessage
                setEditStatusMessage={setEditStatusMessage}
                editMessage={editMessage}
                editedMessage={editedMessage}
            />}
            {deletedMessage &&
            <DeleteMessage
                setDeleteStatusMessage={setDeleteStatusMessage}
                deleteMessage={deleteMessage}
                deletedMessage={deletedMessage}
            />}
        </div>
    );
};

Chat.propTypes = {
    messages: PropTypes.array,
    countMessages: PropTypes.number,
    countUsers: PropTypes.number,
    dateLastMessage: PropTypes.string,
    user: PropTypes.object,
    isLoaded: PropTypes.bool,
    loadUser: PropTypes.func.isRequired,
    loadMessages: PropTypes.func.isRequired,
    addMessage: PropTypes.func.isRequired,
    editMessage: PropTypes.func.isRequired,
    deleteMessage: PropTypes.func.isRequired,
    reactMessage: PropTypes.func.isRequired
};

const actions = {
    loadUser,
    loadMessages,
    addMessage,
    editMessage,
    deleteMessage,
    reactMessage
};

const mapStateToProps = rootState => ({
    messages: rootState.chat.messages,
    countMessages: rootState.chat.countMessages,
    countUsers: rootState.chat.countUsers,
    dateLastMessage: rootState.chat.dateLastMessage,
    isLoaded: rootState.chat.isLoaded,
    user: rootState.user
});

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
