import {
    SET_MESSAGES,
    ADD_MESSAGE,
    DELETE_MESSAGE
} from './actionTypes';

const initialState = {
    messages: [],
    countUsers: 0,
    countMessages: 0,
    dateLastMessage: '',
    isLoaded: false
};

// eslint-disable-next-line
export default (state = initialState, {type, data}) => {
    switch (type) {
        case SET_MESSAGES: {
            return {
                ...state,
                messages: [...data],
                countMessages: data.length,
                countUsers: new Set(data.map(message => message.userId)).size,
                dateLastMessage: data[data.length - 1].createdAt,
                isLoaded: true
            }
        }
        case ADD_MESSAGE: {
            const newMessage = [...state.messages, data];
            return {
                ...state,
                messages: newMessage,
                countMessages: newMessage.length,
                countUsers: new Set(newMessage.map(message => message.userId)).size,
                dateLastMessage: data.createdAt
            }
        }
        case DELETE_MESSAGE: {
            const newMessages = state.messages.filter(message => message.id !== data);
            return {
                ...state,
                messages: newMessages,
                countMessages: newMessages.length,
                countUsers: new Set(newMessages.map(message => message.userId)).size,
                dateLastMessage: newMessages[newMessages.length -1].createdAt
            }
        }
        default: {
            return state
        }
    }
};
