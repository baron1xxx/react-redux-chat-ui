import {createStore, combineReducers, applyMiddleware} from "redux";
import thunk from 'redux-thunk';
import { composeWithDevTools } from "redux-devtools-extension";

import chatReducer from './containers/Chat/reducer'
import profileReducer from './containers/Profile/reducer'

const initialState = {};

const compose = composeWithDevTools({});

const middlewares = [
    thunk
];


const composedEnhancers = compose(
    applyMiddleware(...middlewares)
);

const reducers = {
    chat: chatReducer,
    user: profileReducer
};

const rootReducer = combineReducers({ ...reducers });

const store = createStore(rootReducer, initialState, composedEnhancers);

export default store;
