import {v4 as uuid} from 'uuid';
import moment from 'moment'

export const getMessages = () => {
    return fetch('https://edikdolynskyi.github.io/react_sources/messages.json')
        .then(data => data.json())
        .then(data => {
            return data.map(item => {
                return {
                    ...item,
                    createdAt: moment(item.createdAt).format('DD-MMM-YYYY HH:mm:ss'),
                    isLiked: false
                }
            })
        })
};


export const addMessage = (messageText, authUser) => {
    const {userId, user, avatar} = authUser;
    return {
        id: uuid(),
        userId,
        user,
        avatar,
        text: messageText,
        createdAt: moment().format('DD-MMM-YYYY HH:mm:ss'),
        editedAt: moment().format('DD-MMM-YYYY HH:mm:ss'),
    }
};
