import React from 'react';

import styles from "./styles.module.css";

const Logo = ({src}) => {
    return (
        <div>
            <img className={styles.imgLogo} src={src} alt="Logo"/>
        </div>
    );
};

export default Logo;

