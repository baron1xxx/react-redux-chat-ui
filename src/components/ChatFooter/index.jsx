import React from 'react';
import PropTypes from 'prop-types';

import styles from "./styles.module.css";
import AddMessage from "../AddMessage";

const ChatFooter = ({addMessage, authUser, inputRef}) => {
    return (
        <div className={styles.chatFooterContainer}>
            <AddMessage addMessage={addMessage} authUser={authUser} inputRef={inputRef}/>
        </div>
    );
};

ChatFooter.propTypes = {
    addMessage: PropTypes.func.isRequired,
    authUser: PropTypes.object.isRequired,
    inputRef: PropTypes.object.isRequired
};

export default ChatFooter;
