import React from 'react';
import PropTypes from 'prop-types';

import styles from "./styles.module.css";

const DeleteMessage = ({setDeleteStatusMessage, deleteMessage, deletedMessage}) => {

    const handleDeleteMessage = messageId => {
        deleteMessage(messageId);
        setDeleteStatusMessage(undefined);
    };

    const handleCloseModal = () => {
        setDeleteStatusMessage(undefined);
    };

    return (
        <div className={styles.modalOverlay} >
            <div className={styles.deleteMessageContainer}>
                    <button className={styles.cancelButton} onClick={handleCloseModal}>
                        <div className={styles.icon}><i className="fas fa-window-close"/></div>
                    </button>
                    <button className={styles.submitButton} type='submit' onClick={() => handleDeleteMessage(deletedMessage)}>
                        <div className={styles.icon}><i className="fas fa-trash-alt"/></div>
                    </button>
            </div>
        </div>
    );
};

DeleteMessage.propTypes = {
    deletedMessage: PropTypes.string,
    deleteMessage: PropTypes.func.isRequired,
    setDeleteStatusMessage: PropTypes.func.isRequired
};

export default DeleteMessage;
