import React, {useState} from 'react';
import PropTypes from 'prop-types';

import styles from "./styles.module.css";

const AddMessage = ({inputRef, addMessage, authUser}) => {

    const [text, setText] = useState('');

    const messageBodyChanged = data => {
        setText(data);
    };

    const handleAddMessage = () => {
        addMessage(text, authUser);
        setText('');
    };

    return (
        <div className={styles.addMessageContainer}>
            <form className={styles.formContainer}>
                <textarea
                    ref={inputRef}
                    className={styles.textMessage}
                    name="text"
                    value={text}
                    placeholder="Type your message..."
                    onChange={ev => messageBodyChanged(ev.target.value)}
                >
                </textarea>
                <button className={styles.submitButton} type='submit' disabled={!text} onClick={handleAddMessage}>
                    <div className={styles.sendIcon}><i className="fas fa-paper-plane" /></div>
                </button>
            </form>
        </div>
    );
};

AddMessage.propTypes = {
    addMessage: PropTypes.func.isRequired,
    authUser: PropTypes.object.isRequired,
    inputRef: PropTypes.object.isRequired
};

export default AddMessage;
