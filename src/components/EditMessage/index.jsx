import React, {useState} from 'react';
import PropTypes from 'prop-types';

import styles from "./styles.module.css";

const EditMessage = ({editedMessage, editMessage, setEditStatusMessage}) => {

    const [text, setText] = useState(editedMessage.text);

    const messageBodyChanged = (data) => {
        setText(data);
    };

    const handleCloseModal = () => {
        setEditStatusMessage(undefined);
    };

    const handleEditMessage = (ev) => {
        ev.preventDefault();
        editMessage(text, editedMessage.id);
        setEditStatusMessage(undefined);
    };

    return (
        <div className={styles.modalOverlay} >
            <div className={styles.addMessageContainer}>
                <form className={styles.formContainer}>
                    <button className={styles.cancelButton} onClick={handleCloseModal}>
                        <div className={styles.sendIcon}><i className="fas fa-window-close"/></div>
                    </button>
                    <textarea
                        className={styles.textMessage}
                        value={text}
                        placeholder="Type your message..."
                        onChange={ev => messageBodyChanged(ev.target.value)}
                    >
                    </textarea>
                    <button className={styles.submitButton} type='submit' disabled={!text} onClick={handleEditMessage}>
                        <div className={styles.sendIcon}><i className="fas fa-paper-plane"/></div>
                    </button>
                </form>
            </div>
        </div>
    );
};


EditMessage.propTypes = {
    editedMessage: PropTypes.object,
    editMessage: PropTypes.func.isRequired,
    setEditStatusMessage: PropTypes.func.isRequired
};

export default EditMessage;
