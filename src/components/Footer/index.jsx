import React from 'react';

import styles from "./styles.module.css";

const Footer = () => {
    return (
        <footer className={styles.footer}>
            © Created by Roman Mykytka 2021
        </footer>
    );
};

export default Footer;
