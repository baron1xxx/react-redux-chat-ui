import React, {useRef, useEffect} from 'react';
import PropTypes from 'prop-types';
import Message from "../Message";

import styles from "./styles.module.css";


const MessagesList = ({messages, authUser, reactMessage, setEditStatusMessage, setDeleteStatusMessage}) => {
    const messagesRef = useRef(null);
    useEffect(() => {
        if (messagesRef.current) {
            messagesRef.current.scrollTo(0, 999999);
        }
    }, [messages]);
    return (
        <div ref={messagesRef} className={styles.messagesContainer}>
            {messages.map(({id, user, userId, avatar, text, isLiked, createdAt}) => (
                <Message
                    key={id}
                    id={id}
                    user={user}
                    userId={userId}
                    avatar={avatar}
                    text={text}
                    isLiked={isLiked}
                    date={createdAt}
                    authUser={authUser}
                    reactMessage={reactMessage}
                    setEditStatusMessage={setEditStatusMessage}
                    setDeleteStatusMessage={setDeleteStatusMessage}
                />
            ))}
        </div>
    );
};

MessagesList.propTypes = {
    messages: PropTypes.array.isRequired,
    authUser: PropTypes.object.isRequired,
    reactMessage: PropTypes.func.isRequired,
    setEditStatusMessage: PropTypes.func.isRequired,
    setDeleteStatusMessage: PropTypes.func.isRequired
};

export default MessagesList;
