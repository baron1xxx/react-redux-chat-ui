import React from 'react';
import PropTypes from 'prop-types';

import styles from "./styles.module.css";
import MessagesList from "../MessagesList";

const Modal = ({title, isOpen, onCancel, onSubmit, children}) => {
    return (
        <>
            { isOpen &&
                <div className={styles.modalOverlay}>
                    <div className={styles.modalWindow}>
                        <div className={styles.modalHeader}>
                            <div className={styles.modalTitle}></div>
                            <i className="fas fa-window-close" onClick={onCancel}></i>
                        </div>
                        <div className={styles.modalBody}>
                            {children}
                        </div>
                        <div className={styles.modalFooter}>
                            <button className={styles.cancelButton}>Cancel</button>
                            <button className={styles.submitButton}>Submit</button>

                        </div>

                    </div>
                </div>
            }
        </>

    );
};

MessagesList.propTypes = {
    title: PropTypes.string,
    isOpen: PropTypes.bool,
    onCancel: PropTypes.func,
    onSubmit: PropTypes.func,
    children: PropTypes.node
};

MessagesList.defaultProps = {
    title: 'Modal Title',
    isOpen: false,
    onCancel: () => {
    },
    onSubmit: () => {
    },
    children: null
};

export default Modal;
